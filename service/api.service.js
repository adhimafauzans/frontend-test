import axios from 'axios';
const https = require('https');

export const Api = {
    init() {
        let service = axios.create({
            baseURL: "https://api-axi.spotqoe.com/catalogs",
            headers: {
              'Platform-ID': '3',
            },
        });

        service.interceptors.request.use(
            config => {
                var token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvYXBpLWF4aS5zcG90cW9lLmNvbVwvdXNlcnNcL2F1dGhcL2xvZ2luIiwiaWF0IjoxNTc3NzYwNzc1LCJleHAiOjE1Nzc4NDcxNzUsIm5iZiI6MTU3Nzc2MDc3NSwianRpIjoidUdMcEdQQ1JHN1dqRTgyQiIsInN1YiI6MjYsInBydiI6IjIzYmQ1Yzg5NDlmNjAwYWRiMzllNzAxYzQwMDg3MmRiN2E1OTc2ZjcifQ.Z4VYW-0p45SFN2uekND29SXyqmK0FqLUpOnKYSz-3aI";

                if(token){
                    // add token bearer 'Authorization'
                }
                return config;
            },
            error => Promise.reject(error)
        );

        service.interceptors.response.use(this.handleSuccess, this.handleError);
        this.service = service;
    },
    handleSuccess(response) {
        return response;
    },
    handleError(error) {
        if (error.response == undefined) {
          return Promise.reject("Connection error")
        }

        let err = ''
        if (typeof error.response.data.errors == 'string') {
          err = error.response.data.errors
        } else {
          err = error.response.data.errors[0]
        }

        if (err == undefined) {
          err = error.response.data.message
        }

        return Promise.reject(err)
    },
    get(path) {
        return this.service.get(path)
    },
    post(path, payload) {
        return this.service.request({
            method: 'POST',
            url: path,
            responseType: 'json',
            data: payload
        });
    },
    put(path, payload) {
        return this.service.request({
            method: 'PUT',
            url: path,
            responseType: 'json',
            data: payload
        })
    },
    delete(path, payload) {
        return this.service.request({
            method: 'DELETE',
            url: path,
            responseType: 'json',
            data: payload
        });
    }
}


export const ApiService = {
    init(){
        Api.init()
    }
}


