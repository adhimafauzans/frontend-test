import {Api} from '@/service/api.service'

export const DeliveryServiceSvc = {
  getList() {
    // endpoint get : 'delivery-services?page=&perpage=10&name='
  },
  getDetail() {
    // endpoint get : '/delivery-services/:id'
  },
  insert() {
    // endpoint post : '/delivery-services/:id' , params
    // params :
    // {
    //   "name": "LIDSA",
    //   "description": "Lorem Ipsum Dolor Sit Amet",
    //   "phone_number": "012345",
    //   "pic": "Dadang",
    //   "pic_phone_number": "012345",
    //   "is_active": 1
    // }

  },
  update() {
    // endpoint put : '/delivery-services/:id' , params
    // params :
    // {
    //   "name": "LIDSA",
    //   "description": "Lorem Ipsum Dolor Sit Amet",
    //   "phone_number": "012345",
    //   "pic": "Dadang",
    //   "pic_phone_number": "012345",
    //   "is_active": 1
    // }
  }
}

export const ArtistSvc = {
  getLuke() {
    return {"name":"Luke Skywalker","height":"172","mass":"77","hair_color":"blond","skin_color":"fair","eye_color":"blue","birth_year":"19 June 2010","gender":"male","homeworld":"https://swapi.co/api/planets/1/",
    "films":[{"title": "A New Hope","episode_id": 4,"director": "George Lucas","producer": "Gary Kurtz, Rick McCallum","release_date": "25 April 1994"},{"title": "Attack of the Clones","episode_id": 2,"director": "George Lucas","producer": "Rick McCallum","release_date": "16 Agustus 2005"},{"title": "The Phantom Menace","episode_id": 1,"director": "George Lucas","producer": "Rick McCallum","release_date": "19 March 1995"},{"title": "Revenge of the Sith","episode_id": 3,"director": "George Lucas","producer": "Rick McCallum","release_date": "19 April 2001"},{"title": "Return of the Jedi","episode_id": 6,"director": "Richard Marquand","producer": "Howard G. Kazanjian, George Lucas, Rick McCallum","release_date": "25 November 1999"},{"title": "The Empire Strikes Back","episode_id": 5,"director": "Irvin Kershner","producer": "Gary Kurtz, Rick McCallum","release_date": "25 November 1999"},{"title": "Return of the Jedi","episode_id": 6,"director": "Richard Marquand","producer": "Howard G. Kazanjian, George Lucas, Rick McCallum","release_date": "25 November 1999"},{"title": "The Empire Strikes Back","episode_id": 5,"director": "Irvin Kershner","producer": "Gary Kurtz, Rick McCallum","release_date": "25 November 1999"
    }],"species":["https://swapi.co/api/species/1/"],"vehicles":["https://swapi.co/api/vehicles/14/","https://swapi.co/api/vehicles/30/"],"starships":["https://swapi.co/api/starships/12/","https://swapi.co/api/starships/22/"],"created":"2014-12-09T13:50:51.644000Z","edited":"2014-12-20T21:17:56.891000Z","url":"https://swapi.co/api/people/1/"}
  }
}



